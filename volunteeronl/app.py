import logging
from pathlib import Path
from os.path import dirname
from collections import defaultdict
from dataclasses import dataclass, field
from urllib.parse import urlparse
from frontmatter import load as frontmatter_load
from flask import Flask, g, render_template
from jinja2.utils import markupsafe
from markdown import Markdown

logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.NOTSET)

app = Flask(__name__)

## Define some "models"
@dataclass
class Project:
    """Class of a project"""
    name: str
    description: str
    website: str
    categories: list[str]= field(default_factory=list)
    volunteer_page: str=""
    organization_type: str=""
    license: str=""
    pinned: str=""

@dataclass
class Category:
    name: str
    entries: list[Project]
    pinned: Project=None

@dataclass
class DB:
    projects: dict[Project] = field(default_factory=dict)
    categories: dict[Category] = field(default_factory=dict)

## Create our "database"
def get_db(entries='entries'):
    """Get the 'database' entries and attach them to the application"""
    if 'db' in g:
        # Nothing to do here
        return g.db
    projects = defaultdict(Project)
    categories = defaultdict(list)

    entry_glob = Path(entries).glob('*.md')
    entry_files = [f for f in entry_glob if f.is_file()]

    for fname in entry_files:
        entry = frontmatter_load(fname)
        # An entry has metadata (frontmatter) and content (markdown)
        project = Project(description=entry.content, **entry.metadata)
        project.categories = [x.strip() for x in project.categories.split(',')]
        projects[project.name] = project
        for category in project.categories:
            categories[category].append(project)
    return DB(projects=projects, categories=categories)

def create_app():
    app = Flask(__name__,
                static_folder='assets')
    
    with app.app_context():
        app.db = get_db()
    return app

app = create_app()

@app.template_filter()
def slugify(s):
    s = s.lower().replace(' ', '-')
    return s

@app.template_filter()
def license_link(lic):
    licenses = {
        'CC BY 2.5': 'https://creativecommons.org/licenses/by/2.5/',
        'CC BY-SA 3.0': 'https://creativecommons.org/licenses/by-sa/3.0/',
        'Public Domain': '[https://en.wikipedia.org/wiki/Public_domain',
        'ODbL': 'https://opendatacommons.org/licenses/odbl/',
    }
    return licenses.get(lic, "")

@app.template_filter()
def org_type_link(s):
    org_types = {
        '501c3': "https://en.wikipedia.org/wiki/501(c)(3)_organization)",
    }
    return org_types.get(s, "")

@app.template_filter()
def website(url):
    parsed = urlparse(url)
    scheme = "%s://" % parsed.scheme
    url = parsed.geturl().replace(scheme, '', 1)
    if url[-1] == '/':
        return url[:-1]
    return url

@app.template_filter()
def markdown(text):
    md = Markdown()
    return markupsafe.Markup(md.convert(text))

@app.route("/")
def index():
    project_list = [app.db.projects[p] for p in sorted(app.db.projects.keys())]
    return render_template('index.html',
                           projects=project_list,
                           categories=app.db.categories)

@app.route('/contributing/')
def contributing():
    return render_template('contributing.html')

@app.route('/categories/')
def by_category():
    project_list = [app.db.projects[p] for p in sorted(app.db.projects.keys())]
    return render_template('category.html', projects=project_list,
                           categories=app.db.categories)

@app.route('/alphabetical/')
def by_alphabetical():
    project_list = [app.db.projects[p] for p in sorted(app.db.projects.keys())]
    return render_template('alphabetical.html', projects=project_list)

