---
name: NYPL Map Warper
website: http://maps.nypl.org/warper/
categories: Mapping, History
license: Public Domain
---

The New York Public License Map Warper tool allows volunteers to digitially re-align (ie "rectify") maps from the past onto today's more precise maps, giving visitors and historians a better idea of the geography of New York City.
