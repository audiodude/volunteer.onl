---
name: Mozilla Location Service
website: https://location.services.mozilla.com
license: CC0
categories: Mapping
---

The Mozilla Location service operates an as an alternative to proprietary location services from companies like Google and Apple, allowing an application to know your phyical location, which is useful for everything from directions to driving applications.

Contributing is as easy as running one of the client applications on your phone or laptop and moving around.
