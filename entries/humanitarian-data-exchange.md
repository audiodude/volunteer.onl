---
name: Humanitarian Data Exchange
website: https://data.humdata.org
categories: Disaster
license: Multiple
---

Humanitarian Data Exchange (HDX) is a platform for extracting critical data from documents, many of which are in PDF form. Volunteers go through this data, marking it with online tools that make it easier for organizations and individuals to work with this data and collaborate.

HDX's back end software is Free/Open Source and the data it works with is under a variety of licenses, explained at [https://data.humdata.org/about/license](https://data.humdata.org/about/license).

