---
name: Ancestry World Archives Program
website: https://blogs.ancestry.com/worldarchivesproject/
categories: Genelogy, History, Transcription
organization_type: Commercial
license: CC BY 2.5
---
Ancestry.com runs a project to allow volunteers to transcribe genelogy data into a format that can use used to search and index information about people's families.

It appears that the data is under the Creative Commons Attribution-Only license, but in order to interact with the data, volunteers must run proprietary software.
