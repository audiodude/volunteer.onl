---
name: SCORE
website: https://score.org
volunteer_page: https://www.score.org/volunteer
categories: Entrepreneurship
---

SCORE offers free business education to small businesses around the United States. To do this, they rely on volunteer mentors, subject matter experts, presenters and local chapter support.
