---
name: The Red Cross
website: https://redcross.org
volunteer_page: https://www.redcross.org/volunteer/volunteer-role-finder.html
categories: Disaster
---

The Red Cross has many opportunities for volunteers, including ones you can do online.
