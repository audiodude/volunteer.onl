Contributing
=============

The Good Works Online project welcomes contributions to its database of projects. 

Contributions can take many forms, including

- Adding New Project
- Correcting typos/grammar
- Improving the layout or project structure
- Something else we haven't thought of yet

Guidelines for Contributions
-----------------------------

- Projects must be for volunteer positions, not paid jobs
- Projects must be open-ended, not temporary or meant to fulfill a specific role
- Projects should require little/no prior experience or special talents. This eliminates projects related to software development.
- Projects should be for the benefit of the public good. This means that output from the project must be a [Free Work](https://freedomdefined.org/Definition)
- If the project does not produce an accessible good (eg teaching, mentoring), it must be run by a recognized charitable entity
- Projects that appear dead/inactive may be removed

How to Contribute
------------------

- To add a new entry, look at the "entries" section and either add a new file or edit an existing one.
